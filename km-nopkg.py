import numpy as np


def euclidean_distance(x1, x2):
    """
    Computes the Euclidean distance between two points x1 and x2.
    """
    return np.sqrt(np.sum((x1 - x2) ** 2))


class KMeans:
    """
    KMeans clustering algorithm implementation.
    """

    def __init__(self, k=3, max_iters=100):
        self.k = k
        self.max_iters = max_iters

    def initialize_centroids(self, X):
        """
        Initializes k centroids by randomly selecting k data points from X.
        """
        m, n = X.shape
        centroids = np.zeros((self.k, n))
        idx = np.random.choice(m, self.k, replace=False)
        print(idx)
        for i in range(self.k):
            centroids[i] = X[idx[i]]
        return centroids

    def assign_clusters(self, X, centroids):
        """
        Assigns each data point in X to the closest centroid.
        """
        m = X.shape[0]
        cluster_assignments = np.zeros(m)
        for i in range(m):
            distances = [euclidean_distance(X[i], centroid) for centroid in centroids]
            cluster_assignments[i] = np.argmin(distances)
        return cluster_assignments

    def compute_centroids(self, X, cluster_assignments):
        """
        Computes the new centroids for each cluster based on the data points assigned to them.
        """
        centroids = np.zeros((self.k, X.shape[1]))
        for i in range(self.k):
            cluster_points = [X[j] for j in range(X.shape[0]) if cluster_assignments[j] == i]
            centroids[i] = np.mean(cluster_points, axis=0)
        return centroids

    def fit(self, X):
        """
        Fits the KMeans model to the data X.
        """
        self.centroids = self.initialize_centroids(X)
        for i in range(self.max_iters):
            cluster_assignments = self.assign_clusters(X, self.centroids)
            new_centroids = self.compute_centroids(X, cluster_assignments)
            if np.allclose(self.centroids, new_centroids):
                break
            self.centroids = new_centroids
        return self

    def predict(self, X):
        """
        Predicts the cluster assignments for each data point in X based on the fitted model.
        """
        return self.assign_clusters(X, self.centroids)

X = np.array([[1, 2], [1, 4], [1, 0], [4, 2], [4, 4], [4, 0],[0, 0],[0,4],[6,6],[33,33]])
kmeans = KMeans(k=4)
kmeans.fit(X)
print(kmeans.predict(X))
